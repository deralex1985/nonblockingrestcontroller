package com.example;


import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
public class Controller {

    private int requestCounter = 0;

    // sample Request: http://localhost:8080/sum?firstNumber=3&secondNumber=7 , does not work with Chrome as it enforces SSL
    @Async
    @GetMapping(path = "/sum")
    private CompletableFuture<String> getSum(
            @RequestParam(name = "firstNumber") int firstNumber,
            @RequestParam(name = "secondNumber") int secondNumber) {
        requestCounter++;
        try {
            if (requestCounter % 2 != 0) {
                log.info("Sleeping for 15 seconds. Simulating very longlasting Computation ({}+{})", firstNumber, secondNumber);
                Thread.sleep(15000);
            }
            return CompletableFuture.completedFuture(String.valueOf(firstNumber + secondNumber));
        } catch (Exception e) {
            log.warn("Wrong Input, error is {}", e.getMessage());
            return CompletableFuture.completedFuture("Sorry, your input was wrong, please try again");
        }
    }

    @GetMapping(path = "/echo")
    private String echoParam(@RequestParam(name = "text") String textFromUser) {
        return "This is what you send us: \n \n" + textFromUser;

    }

    @PostMapping(path = "/echo")
    private String echoBody(@RequestBody String textFromUser) {
        return "This is what you send us: \n \n" + textFromUser;
    }
}
