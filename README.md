# NonBlockingRestController

Simple ShowCase of Non Blocking RestController with Spring Boot. Computation Method uses Thread.sleep to simulate longlasting computation on every second request.
